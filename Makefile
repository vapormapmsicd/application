all: auth pull

auth:
	@docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"

pull:
	@docker pull "${BACKEND_IMAGE_TAG}"
	@docker image tag "${BACKEND_IMAGE_TAG}" backend:latest
	@docker rmi "${BACKEND_IMAGE_TAG}"
	@docker pull "${FRONTEND_IMAGE_TAG}"
	@docker image tag "${FRONTEND_IMAGE_TAG}" frontend:latest
	@docker rmi "${FRONTEND_IMAGE_TAG}"

compose:
	@docker-compose up --detach

clean:
	@docker rmi backend:latest frontend:latest

git-clone:
	@echo "Cloning ansible configs and kubernetes manifests"
	@echo "delete ansible-config directory\n" && if [ -d /home/gitlab-runner/ansible-config ];then rm -r /home/gitlab-runner/ansible-config;fi
	@git clone "${ANSIBLE_CONFIG_URL}" /home/gitlab-runner/ansible-config 
	@echo "delete kubernetes directory\n" && if [ -d /home/gitlab-runner/kubernetes ];then rm -r /home/gitlab-runner/kubernetes;fi
	@git clone "${KUBERNETS_CONFIG_URL}" /home/gitlab-runner/kubernetes


ansible-help:
	@ansible-playbook -h

set-env:
	@ansible-playbook -i "${ANSIBLE_HOSTS}" --private-key "${ANSIBLE_SSH_KEY}" "${ANSIBLE_PLAYBOOKS}"/install.yaml
	@ansible-playbook -i "${ANSIBLE_HOSTS}" --private-key "${ANSIBLE_SSH_KEY}" "${ANSIBLE_PLAYBOOKS}"/pre-config.yaml
	@ansible-playbook -i "${ANSIBLE_HOSTS}" --private-key "${ANSIBLE_SSH_KEY}" "${ANSIBLE_PLAYBOOKS}"/enable-addons.yaml
	@ansible-playbook -i "${ANSIBLE_HOSTS}" --private-key "${ANSIBLE_SSH_KEY}" "${ANSIBLE_PLAYBOOKS}"/copy.yaml

deploy:
	@ansible-playbook -i "${ANSIBLE_HOSTS}" --private-key "${ANSIBLE_SSH_KEY}" "${ANSIBLE_PLAYBOOKS}"/deploy.yaml

test:
	@ansible-playbook -i "${ANSIBLE_HOSTS}" --private-key "${ANSIBLE_SSH_KEY}" "${ANSIBLE_PLAYBOOKS}"/test.yaml