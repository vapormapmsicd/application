# Description
This project is a web application for training purposes developed by `Tristan Le Toullec`. With this web application, you can record the geographical coordinates (or GPS points) with a simple click or by manual entries. Then it displays this location on a map.<br>

The application's architecture has three main components: the frontend, backend, and database. Each part runs on different servers but communicates to provide the end-users with the services needed. The frontend is a static website developed with `HTML`, `CSS` and `Javascript` and can be deployed on an Nginx web server. Nginx will also serve as a reverse proxy to redirect the requests to the frontend and backend. The backend is developed in `Python` with the framework `Django`. The communication between the Nginx server and the backend is handled by `Gunicorn`. It is a Python Web Server Gateway Interface (WSGI) HTTP server that supports `Django` natively. The geographical coordinates are stored in a `SQL` database managed by a `MariaDB` server, an open-source `MySQL` relational database management system. The figure below illustrates the global architecture of the application.


<img src=./img/vapormap-app.png width="350" height="400" title="Block diagram of the proposed architecture" style="margin-left: 300px;"/>

# How to deploy
We propose two modes for deploying the vapormap application: deployment on development environment with **docker-compose** and deployment on production with **kubernetes**.  
Below are the steps to deploy the Vapomap application both on the development and production environments.

1. **Prepare the depolyment's environments**:
```shell
URL=https://gitlab.com/gbazack/vapormap.git
git clone $URL vapormap
```

2. **Developement environment**:<br>
Make sure you update the environment variables in `.env` file. Then you can deploy the application with the commands below.
```shell
cd vapormap/
docker-compose --env-file .env up -d
```

If your developement environment is on your local computer, test the backend with command:
```shell
curl http://localhost:8000/api/?format=json
```

Expected output:
```shell
{"points":"http://localhost:8000/api/points/?format=json"}
```

3. **Production environment**:

The production environment is kubernetes cluster. You can refer to the repository [cloud-orchestration](https://gitlab.com/gbazack/cloud-orchestration/-/tree/main/terraform/vapormap "Click here!") for more details on how to set up the environment.

- **Create a gitlab repository**:<br>
If you are using the production environment proposed in [cloud-orchestration](https://gitlab.com/gbazack/cloud-orchestration/-/tree/main/terraform/vapormap "Click here!"), therefore you must create a repository on Gitlab, called `vapormap`, with a copy of the vapormap application (see [create a repository](https://docs.gitlab.com/ee/user/project/repository/, "Click here!")).<br>

- **Update the environment variables**:<br>
Once you have created a copy of the vapormap repository, you must add the variables in file `.env` to your gitlab project ([Add a CI/CD variable to a project](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project, "Click here")).

- **Deploy**:<br>
```shell
git commit --allow-empty -m "Empty commit"
git push origin dev
```