# Install and configure Gitlab-runner

## Docker

1. Download gitlab-runner docker image
```shell
docker pull gitlab/gitlab-runner:latest
```

2. Create a Docker volume to save the configuration
```shell
 docker volume create gitlab-runner-config
 ```

3. Start the GitLab Runner container using the volume created
```shell
docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest
```

4. Configuration
```shell
docker exec -it gitlab-runner sh
apt update && apt upgrade -y
apt install docker docker-compose -y
apt install make ansible-y
gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
(runner executor= shell)
cat /etc/password (to list all users)
usermod -aG docker gitlab-runner
chmod 666 /var/run/docker.sock (optional)
exit
docker restart gitlab-runner
docker exec -it gitlab-runner sh 
gitla-runner start
```
